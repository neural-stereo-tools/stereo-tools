import logging
from itertools import groupby
from pathlib import Path
from types import SimpleNamespace

import click
import holoviews as hv
import numpy as np
from bokeh.plotting import show

from stereo_tools.utils import pfm

hv.extension('bokeh')

logger = logging.getLogger(__name__)


RESOLUTIONS = ['vga', 'hd720', 'hd1080', 'hd2k']
SENSING_MODES = ['medium', 'quality', 'performance']
FILL_MODE = ['fill', 'standard']


class FileInfo(SimpleNamespace):
    path: Path
    method: str
    distance: float
    type: str
    resolution: str
    num: int

    @staticmethod
    def from_path(path) -> 'FileInfo':
        METHOD = 0
        RESOLUTION = 1
        SCENE = 2
        DISTANCE = 3
        TYPE = 4
        NUM = 5

        parts = path.stem.split('_')
        return FileInfo(
            path=path,
            method=parts[METHOD],
            distance=float(parts[DISTANCE]),
            type=parts[TYPE],
            resolution=parts[RESOLUTION],
            scene=parts[SCENE],
            num=int(parts[NUM])
        )


class FilePair(SimpleNamespace):
    disp: FileInfo
    depth: FileInfo

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        if (self.disp.path.parent != self.depth.path.parent or
                self.disp.path.suffix != self.depth.path.suffix):
            raise ValueError(
                f'Invalid file pairt: {self.disp.path} != {self.depth.pair}'
            )

    @staticmethod
    def from_file(file_info: FileInfo) -> 'FilePair':
        path = file_info.path
        if path.suffix != '.pfm':
            raise ValueError(f'Unknown extension of {path}')

        if file_info.type not in ['depth', 'disp']:
            raise ValueError(f'Unknown file content in {path}')

        if file_info.type == 'depth':
            return FilePair(
                disp=FileInfo.from_path(
                    Path(str(path).replace('depth.pfm', 'disp.pfm'))),
                depth=file_info,
            )
        return FilePair(
            disp=file_info,
            depth=FileInfo.from_path(
                Path(str(path).replace('disp.pfm', 'depth.pfm'))),
        )


class Measure(SimpleNamespace):
    info: FileInfo
    value: float
    x: int
    y: int

    @staticmethod
    def from_path(fi: FileInfo) -> 'Measure':
        data, _ = pfm.load_pfm(fi.path)
        height, width = data.shape

        center_y = int(height / 2)
        center_x = int(width / 2)
        return Measure(
            info=fi,
            value=float(data[center_y, center_x]),
            x=center_x,
            y=center_y,
        )


class RecordMeasure(SimpleNamespace):
    disp: Measure
    depth: Measure

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        if (self.disp.x != self.depth.x or
                self.disp.y != self.depth.y):
            raise ValueError(
                f'Position in Image does not match: {self.disp} != {self.depth}',
            )

    @property
    def info(self) -> FileInfo:
        return self.disp.info

    @staticmethod
    def from_files(fp: FilePair) -> 'RecordMeasure':
        return RecordMeasure(
            depth=Measure.from_path(fp.depth),
            disp=Measure.from_path(fp.disp),
        )


def generate_plot(df, title: str, link_to=None):
    plot = None
    for method, data in df.items():
        x, y = data.T
        # error = np.abs((x * 10) - y).mean()

        p = hv.Points(data, label=method)
        p = p.opts(plot={'height': 400, 'width': 600})
        if not plot:
            plot = p
        else:
            plot *= p

    return plot


def generate_error_plot(df, title: str, link_to=None):
    plot_data = []
    for method, data in df.items():
        x, y = data.T
        error = (x * 10) - y
        for data_x, data_y in zip(x, error):
            plot_data.append((data_x, method, data_y))

    plot_data = sorted(plot_data, key=lambda x: (x[1], x[0]))

    b = hv.Bars(plot_data, ['Distance', 'Mode'], 'Error')
    b = b.opts(plot={'height': 400, 'width': 600})
    return b


def group_data_by(data, func):
    sorted_data = sorted(data, key=func)
    yield from groupby(sorted_data, func)


@click.command()
@click.option('--mode', type=click.Choice(['disp', 'depth']), default='depth')
@click.option('--resolution', type=str, default='all')
@click.option('--scene', type=click.Choice(['whitewall', 'sphere', 'edge', 'all']), default='all')
@click.argument('data_dir', type=click.Path(exists=True, dir_okay=True, file_okay=False))
def run(data_dir: str, resolution: str, scene: str, mode: str):
    path = Path(data_dir)

    # filters
    by_resolution = lambda fi: resolution == 'all' or fi.resolution == resolution
    by_scene = lambda fi: scene == 'all' or fi.scene == scene

    # load data
    file_paths = list(path.rglob('*_depth*.pfm'))
    files = map(FileInfo.from_path, file_paths)

    # filter files
    files = filter(by_resolution, files)
    files = filter(by_scene, files)

    # load measure
    file_pairs = map(FilePair.from_file, files)
    measures = map(RecordMeasure.from_files, file_pairs)
    all_data = list(measures)
    if not all_data:
        raise RuntimeError('No measures found...')

    # groupby helpers
    by_resolution = lambda m: m.info.resolution  # NOQA
    by_method = lambda m: m.info.method  # NOQA
    by_num = lambda m: m.info.num  # NOQA

    # charts by resolution
    plots = None  # type: ignore
    for resolution, chart_data in group_data_by(all_data, by_resolution):
        for method, method_data in group_data_by(chart_data, by_method):
            data = {}
            for num, num_data in group_data_by(method_data, by_num):
                t_data = []
                for d in num_data:
                    info = d.info
                    value = getattr(d, mode).value
                    if value == 0:
                        continue

                    t_data.append([info.distance, value])
                    print(info.scene, info.method, info.distance * 10, info.resolution,
                          info.num, value, sep=', ')
                data[f'{method}-{num}'] = np.asarray(t_data)

            error_plot = generate_error_plot(data, f'{method} ({resolution})')
            depth_plot = generate_plot(data, f'{method} ({resolution})')
            plot = depth_plot + error_plot

            if not plots:
                plots = plot
            else:
                plots += plot

    layout = hv.Layout(plots).cols(2)

    renderer = hv.renderer('bokeh')
    layout = renderer.get_plot(layout).state
    show(layout)


if __name__ == '__main__':
    run()
