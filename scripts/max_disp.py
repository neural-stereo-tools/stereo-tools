from pathlib import Path

import click
import numpy as np

from stereo_tools.utils import pfm


@click.command()
@click.argument('data_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
def run(data_dir: str) -> None:
    data_path = Path(data_dir)
    dmin = 10000000
    dmax = 0
    for file_ in data_path.rglob('*disp.pfm'):
        disp, _ = pfm.load_pfm(Path(file_))
        disp = np.abs(disp)
        print(disp.min(), disp.max(), file_)

        dmin = min(dmin, disp.min())
        dmax = max(dmax, disp.max())

    print()
    print(dmin, dmax)


if __name__ == '__main__':
    run()
