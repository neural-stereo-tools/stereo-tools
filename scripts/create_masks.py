import shutil
from pathlib import Path

import click


@click.command()
@click.argument('data_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
@click.argument('output_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
def run(data_dir: str, output_dir: str):
    RESOLUTION = -3
    DISTANCE = -4
    SCENE = -5

    data_path = Path(data_dir)
    output_path = Path(output_dir)
    for file_ in data_path.rglob('left.webp'):

        # normalize
        path = str(file_.parent)
        path = path.replace('white_wall', 'whitewall')
        path = path.replace('spere', 'sphere')

        parts = path.split('/')

        extension = file_.suffix
        nbasename = '_'.join([
            parts[SCENE],
            parts[RESOLUTION],
            parts[DISTANCE],
        ])
        nfilename = f'{nbasename}{extension}'
        target_file = output_path / nfilename

        print(file_, '->', target_file)
        shutil.copyfile(file_, target_file)


@click.command()
@click.argument('data_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
@click.argument('output_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
def run_existing(data_dir: str, output_dir: str):
    RESOLUTION = -1
    DISTANCE = -2
    SCENE = -3

    data_path = Path(data_dir)
    output_path = Path(output_dir)
    for file_ in data_path.rglob('mask*.png'):

        # normalize
        path = str(file_.parent)
        path = path.replace('white_wall', 'whitewall')
        path = path.replace('spere', 'sphere')

        parts = path.split('/')

        extension = file_.suffix
        nbasename = '_'.join([
            parts[SCENE],
            parts[RESOLUTION],
            parts[DISTANCE],
        ])
        suffix = file_.stem.replace('mask', '').replace('.webp', '')
        nfilename = f'{nbasename}{suffix}{extension}'
        target_file = output_path / nfilename

        print(file_, '->', target_file)
        shutil.copyfile(file_, target_file)


if __name__ == '__main__':
    run_existing()
