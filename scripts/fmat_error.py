from pathlib import Path

import click
import cv2
import numpy as np

from stereo_tools import models
from stereo_tools.calibrate import find_chessboard_corners
from stereo_tools.utils import click as types


def make_homogeneous(points):
    points = np.squeeze(points)
    if points.shape[1] != 3:
        return np.hstack((points, np.ones((points.shape[0], 1))))
    return points


def ZED():
    print('ZED')
    M1 = np.asarray([
        [699.844970703125, 0, 648.2100219726562],
        [0, 699.844970703125, 360.3529968261719],
        [0, 0, 1],
    ])
    d1 = np.asarray([-0.17106199264526367, 0.026164300739765167, 0.0, 0.0, 0.0])
    M2 = np.asarray([
        [699.2849731445312, 0, 626.4359741210938],
        [0, 699.2849731445312, 356.12799072265625],
        [0, 0, 1],
    ])
    d2 = np.asarray([-0.1713699996471405, 0.02627049945294857, 0.0, 0.0, 0.0])
    T = np.asarray([119.91300201416016, 0.0, 0.0])
    R = cv2.Rodrigues(
        np.asarray([-0.00259634992107749, 0.014316299930214882, -0.00030825199792161584])
    )[0]
    return M1, d1, M2, d2, T, R


def MY():
    print('MY')
    M1 = np.asarray([
        [696.5275241926638, 0.0, 648.1248952165428],
        [0.0, 696.6552881548076, 362.87857835388155],
        [0.0, 0.0, 1.0],
    ])
    d1 = np.asarray([-0.15935323720805147, -0.00037625090546105966, 0.0, 0.0, 0.01457090169978159])  # NOQA
    M2 = np.asarray([
        [696.9523528017314, 0.0, 629.0116576942085],
        [0.0, 696.999798346865, 357.494776473274],
        [0.0, 0.0, 1.0],
    ])
    d2 = np.asarray([-0.16042939228500525, -0.0048425172360243955, 0.0, 0.0, 0.020852428599738647])  # NOQA
    T = np.asarray([-5.983900591647597, 0.007192576407150826, -0.03460473793256286])
    R = np.asarray([
        [0.9999310437838694, 2.149953259841032e-05, 0.011743390271614286],
        [-6.606127646578778e-05, 0.9999927996304117, 0.003794248705502994],
        [-0.01174322414029038, -0.0037947628518205834, 0.9999238453310778],
    ])
    return M1, d1, M2, d2, T, R


@click.command()
@click.argument('checkerboard', type=types.TupleIntType())
@click.argument('left_image', type=click.Path(exists=True, dir_okay=False))
@click.argument('right_image', type=click.Path(exists=True, dir_okay=False))
def run(checkerboard,
        left_image: str,
        right_image: str) -> None:

    for cam in (MY, ZED):
        M1, d1, M2, d2, T, R = cam()

        # camera json
        # stereo_camera = models.StereoCamera.load(Path(camera_json))
        # d1 = stereo_camera.left.distortion
        # d2 = stereo_camera.right.distortion
        # M1 = stereo_camera.left.matrix
        # M2 = stereo_camera.right.matrix
        # T = stereo_camera.translation_matrix
        # R = stereo_camera.rotation_matrix

        limg = models.Image.load(Path(left_image))
        rimg = models.Image.load(Path(right_image))
        image_data = np.concatenate([
            cv2.undistort(limg.data, M1, d1),
            cv2.undistort(rimg.data, M2, d2),
        ], axis=1)
        image = models.StereoImage.from_image(data=image_data, image=limg)

        left_points = find_chessboard_corners(image.left, checkerboard)
        right_points = find_chessboard_corners(image.right, checkerboard)
        point_count = len(left_points)
        left_points = make_homogeneous(left_points)
        right_points = make_homogeneous(right_points)

        # calc F
        S = np.asarray([
            [0, -T[2], T[1]],
            [T[2], 0, -T[0]],
            [-T[1], T[0], 0],
        ])
        F = np.linalg.inv(M1.transpose()) @ R @ S @ np.linalg.inv(M2)

        err = np.empty(point_count)
        for i, (x1, x2) in enumerate(zip(left_points, right_points)):
            err[i] = x1.T.dot(F).dot(x2)
            # print(err[i])

        print(err)
        print(err.sum())
        print(err.mean())


if __name__ == '__main__':
    run()
