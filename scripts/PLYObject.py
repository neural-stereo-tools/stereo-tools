import plyfile
import numpy
import scipy.optimize

class PLYObject:
    
    def __init__(self,name=None):
        if not name is None: self.read(name)
    
    def read(self,name):
        """
        Liest die PLY-Datei name ein.
        """
        self.plydata=plyfile.PlyData.read(name)
    
    def getVertices(self):
        """
        Liefert die n Knoten des Objekts als 3xn-ndarray.
        """
        return numpy.asarray([self.plydata['vertex'][dim] for dim in ('x','y','z')])
    
    def fitSphere(self):
        v=self.getVertices()
        initial_c=numpy.mean(v,axis=1)
        initial_r=(numpy.mean(numpy.sqrt(numpy.square(v[0]-initial_c[0])+numpy.square(v[1]-initial_c[1])+numpy.square(v[2]-initial_c[2]))))
        func=lambda arg:numpy.abs(numpy.sqrt(numpy.square(v[0]-arg[0])+numpy.square(v[1]-arg[1])+numpy.square(v[2]-arg[2]))-arg[3])
        opt=scipy.optimize.leastsq(func,numpy.append(initial_c,[initial_r]))
        return tuple(opt[0])+(func(opt[0]).mean(),)
    
    def fitPlane(self):
        v=self.getVertices()
        pts=v[:,numpy.random.choice(v.shape[1],3,replace=False)].T
        initial_a=numpy.cross(pts[0]-pts[2],pts[1]-pts[2])
        initial_d=numpy.inner(initial_a,pts[2])
        initial_a/=initial_d
        func=lambda arg:numpy.abs(arg[0]*v[0]+arg[1]*v[1]+arg[2]*v[2]-1)/numpy.sqrt(arg[0]**2+arg[1]**2+arg[2]**2)
        opt=scipy.optimize.leastsq(func,initial_a)
        return tuple(opt[0])+(func(opt[0]).mean(),)


if __name__=="__main__": #Leitet eine Testumgebung ein. Der folgende Code wird bei der Korrektur nicht ausgefuehrt(!) und kann daher beliebig abgeaendert werden.
    a=PLYObject("sphere.ply")
    print(a.fitSphere())
    b=PLYObject("plane.ply")
    print(b.fitPlane())
