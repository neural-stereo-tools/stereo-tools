import csv
import logging
from itertools import groupby
from types import SimpleNamespace
from typing import List

import click
import numpy as np
from bokeh.core.enums import palettes
from bokeh.io import export_svgs
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, Whisker
from bokeh.plotting import figure, show

logger = logging.getLogger(__name__)


class Measure(SimpleNamespace):
    distance: int
    method: str
    values: List[float]

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        if not getattr(self, 'values', None):
            self.values = []


def generate_plot(df, title: str, link_to=None, color: str = 'black', shift: int = 0):

    if link_to:
        p = link_to
    else:
        p = figure(plot_width=16 * 50, plot_height=14 * 50)

    mean, base, lower, upper = [], [], [], []

    for distance, values in df.items():
        lower.append(values.min())  # mean - std)
        upper.append(values.max())  # mean + std)
        base.append(distance + shift)
        mean.append(values.mean())

    source_error = ColumnDataSource(data=dict(base=base, lower=lower, upper=upper))

    # for i, distance in enumerate(df.keys()):
    #     y = df[distance]
    #     p.cross(x=distance + shift, y=y, color=color, line_width=2, size=10)

    p.add_layout(
        Whisker(
            source=source_error, base="base", upper="upper", lower="lower",
            line_width=2,
        )
    )
    p.line(x=base, y=mean, color=color, legend=title, line_width=3)
    p.cross(x=base, y=mean, color='#333333', size=10, line_width=2)

    p.legend.location = "top_left"
    p.xaxis.ticker = list(df.keys())
    p.y_range.end = 55
    p.y_range.start = 0
    p.xaxis.axis_label = 'Entfernung in cm'
    p.yaxis.axis_label = 'Abweichung in cm'

    p.xaxis.axis_label_text_font_size = "14pt"
    p.xaxis.major_label_text_font_size = "14pt"
    p.yaxis.axis_label_text_font_size = "14pt"
    p.yaxis.major_label_text_font_size = "14pt"
    p.legend.label_text_font_size = "14pt"

    return p


@click.command()
@click.option('--limit', type=click.Choice(['zed', 'zed-fill', 'zed-all', 'other', 'all']), default='all')
@click.option('--save', type=click.Path(exists=False))
@click.argument('file_path', type=click.Path(exists=True, dir_okay=False, file_okay=True))
@click.argument('scene', type=click.Choice(['whitewall', 'edge']))
def run(file_path: str, scene: str, limit: str, save: str):
    SCENE = 0
    METHOD = 1
    DISTANCE = 2
    RESOLUTION = 3
    NUM = 4
    VALUE = 5
    ERROR = 6

    data = None
    methods = set()
    with open(file_path) as fh:
        reader = csv.reader(fh)
        for row in reader:
            if data is None:  # skip first row
                data = {}
                continue
            key = (row[METHOD].strip(), row[DISTANCE])
            methods.add(row[METHOD].strip())
            if row[SCENE] == scene:
                method = row[METHOD].strip()
                if limit == 'zed':
                    if 'zed' not in method.lower() or 'fill' in method.lower():
                        continue
                if limit == 'zed-fill':
                    if 'fill' not in method.lower():
                        continue
                if limit == 'zed-all':
                    if 'zed' not in method.lower():
                        continue
                if limit == 'other':
                    if 'zed' in method.lower():
                        continue

                try:
                    m = data[key]
                except KeyError:
                    m = Measure(
                        method=row[METHOD].strip(),
                        distance=float(row[DISTANCE]) / 10,
                    )
                    if m.distance > 350:
                        continue
                    data[key] = m
                m.values.append(float(row[ERROR]) / 10)

    sorted_data = sorted(data.values(), key=lambda x: (x.method, x.distance))
    grouped_data = groupby(sorted_data, key=lambda x: x.method)

    colors = palettes.Category10_10
    color_map = {m: c for m, c in zip(sorted(methods), colors)}

    depth_plots = []
    for i, (method, measures) in enumerate(grouped_data):
        df = {}
        for measure in measures:
            df[measure.distance] = np.asarray(measure.values)

        color = color_map[method]
        method = method.replace('medium', 'M')
        method = method.replace('performance', 'P')
        method = method.replace('quality', 'Q')
        method = method.replace('-standard', '')
        method = method.replace('-fill', '/F')
        method = method.replace('sgbm', 'SGBM')
        method = method.replace('full', '(8 Pfade)')
        method = method.replace('-', ' ')

        depth_plots.append(
            generate_plot(
                df, f'{method}',
                link_to=depth_plots[-1] if depth_plots else None,
                color=color,
                shift=i * 5,
            )
        )
    plot = depth_plots[-1]
    show(plot)

    if save:
        plot.output_backend = "svg"
        export_svgs(plot, filename=save)


if __name__ == '__main__':
    run()
