import logging
import re
from itertools import groupby
from pathlib import Path
from types import SimpleNamespace
from typing import Generator

import click
import numpy as np
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, Whisker
from bokeh.plotting import figure, show

logger = logging.getLogger(__name__)


class Measure(SimpleNamespace):
    depth: float
    disp: float


class File(SimpleNamespace):
    distance: int
    method: str
    content: str


def get_measures(content: str) -> Generator[Measure, None, None]:
    for line in content.splitlines():
        match = re.match(r'.*?:\sdepth=(\d+.\d+),\sdisp=-?(\d+.\d+).', line)
        if match:
            yield Measure(
                depth=float(match.groups()[0]),
                disp=float(match.groups()[1]),
            )


def find_files(path: Path) -> Generator[File, None, None]:
    for file in path.glob('*.log'):
        match = re.match(r'(\d+)_([^\.]+).log', file.name)
        if not match:
            logger.warning(f'Invalid file {file}')
            continue
        groups = match.groups()
        with open(file) as fh:
            yield File(
                distance=int(groups[0]) * 10,
                method=groups[1],
                content=fh.read(),
            )


def generate_plot(df, title: str, link_to=None):
    colors = ["red", "olive", "darkred", "goldenrod", "skyblue", "orange", "salmon"]

    if link_to:
        p = figure(plot_width=600, plot_height=300, title=title,
                   x_range=link_to.x_range, y_range=link_to.y_range)
    else:
        p = figure(plot_width=600, plot_height=300, title=title)

    base, lower, upper = [], [], []

    for distance, values in df.items():
        mean = values.mean()
        std = values.std()
        lower.append(mean - std)
        upper.append(mean + std)
        base.append(distance)

    source_error = ColumnDataSource(data=dict(base=base, lower=lower, upper=upper))

    p.add_layout(
        Whisker(source=source_error, base="base", upper="upper", lower="lower")
    )

    for i, distance in enumerate(df.keys()):
        y = df[distance]
        color = colors[i % len(colors)]
        p.circle(x=distance, y=y, color=color)
        p.x(x=distance, y=y.mean(), color='black')
    return p


@click.command()
@click.argument('data_dir', type=click.Path(exists=True, dir_okay=True, file_okay=False))
def run(data_dir: str):
    files = find_files(Path(data_dir))

    by_method = lambda f: f.method
    sorted_files = sorted(files, key=by_method)
    grouped_files = groupby(sorted_files, by_method)

    depth_plots = []
    disp_plots = []
    for method, method_files in grouped_files:
        df = {
            'depth': {},
            'disp': {},
        }
        for file in method_files:
            measures = list(get_measures(file.content))
            df['depth'][file.distance] = np.asarray([m.depth for m in measures])
            df['disp'][file.distance] = np.asarray([m.disp for m in measures])
        depth_plots.append(generate_plot(df['depth'], f'{method}(depth)',
                                         link_to=depth_plots[-1] if depth_plots else None))
        disp_plots.append(generate_plot(df['disp'], f'{method}(disp)',
                                        link_to=disp_plots[-1] if disp_plots else None))

    show(column(*depth_plots))
    # show(column(*disp_plots))


if __name__ == '__main__':
    run()
