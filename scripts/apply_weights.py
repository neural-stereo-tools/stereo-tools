import os

import click
import numpy as np

from stereo_tools.dnn.predict import get_network


@click.command()
@click.argument('weights_path', type=click.Path(exists=True, file_okay=True,
                                                dir_okay=False))
@click.argument('network_name', type=str)
@click.argument('output_path')
def run(weights_path, network_name, output_path):
    # load neural network
    network_class = get_network(network_name)
    network = network_class(width=768, height=384)
    model = network.build()

    # load weights
    # NOTE we are loading data stored with PY2, hence `encoding='bytes'` is
    #      necessary
    with np.load(weights_path, encoding='bytes') as data:
        weights = {key.decode('utf8'): value
                   for key, value in data['arr_0'][()].items()}

    for layer in model.layers:
        if layer.name in weights:
            print('Set weights for layer `{}`'.format(layer.name))

            # transform weights to tf format
            layer_weights = []
            for w in weights[layer.name]:
                if len(w.shape) == 4:
                    # https://github.com/ry/tensorflow-vgg16/blob/master/caffe_to_tensorflow.py#L66
                    w = w.transpose(2, 3, 1, 0)
                layer_weights.append(w)

            # set weights
            layer.set_weights(layer_weights)
        elif layer.get_weights():
            print('Layer requires weights {}'.format(layer.name))
        else:
            print(layer.get_weights())

    model_path = os.path.join(output_path, 'model.json')
    weights_path = os.path.join(output_path, 'weights.hdf5')

    model.save_weights(weights_path)
    try:
        with open(model_path, 'w') as f:
            f.write(model.to_json())
        print('Model and weights saved to `{}` and `{}`.'.format(
            model_path, weights_path))
    except Exception as e:
        print(f'Could not save model: {e}')


if __name__ == '__main__':
    run()
