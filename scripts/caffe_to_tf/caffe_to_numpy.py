# inspired by
# https://github.com/Ignotus/theano-flownet/blob/master/caffe_to_numpy.py
import os
import sys

import numpy as np
from math import ceil

DISPNET_DIR = '/home/alex/Projects/thirdparty/dispflownet-1.2'
sys.path.append('%s/python' % DISPNET_DIR)

import caffe  # NOQA


if __name__ == '__main__':
    deploy_path = sys.argv[1]
    model_path = sys.argv[2]
    target_file = sys.argv[3]

    model_tmp_dir = '/'.join(model_path.split('/')[:-2])

    height = 384
    width = 768
    divisor = 64.
    adapted_width = ceil(width/divisor) * divisor
    adapted_height = ceil(height/divisor) * divisor
    rescale_coeff_x = width / adapted_width
    rescale_coeff_y = height / adapted_height

    replacement_list = {
        '$ADAPTED_WIDTH': ('%d' % adapted_width),
        '$ADAPTED_HEIGHT': ('%d' % adapted_height),
        '$TARGET_WIDTH': ('%d' % width),
        '$TARGET_HEIGHT': ('%d' % height),
        '$SCALE_WIDTH': ('%.8f' % rescale_coeff_x),
        '$SCALE_HEIGHT': ('%.8f' % rescale_coeff_y),
        'tmp/img1.txt': os.path.join(model_tmp_dir, 'tmp/img1.txt'),
        'tmp/img2.txt': os.path.join(model_tmp_dir, 'tmp/img2.txt'),
    }

    proto = ''
    with open(deploy_path, "r") as tfile:
        proto = tfile.read()

    for r in replacement_list:
        proto = proto.replace(r, replacement_list[r])

    deploy_file = '/tmp/deploy.prototxt'
    with open(deploy_file, "w") as tfile:
        tfile.write(proto)

    caffe.set_mode_gpu()
    caffe.set_device(0)

    net = caffe.Net(deploy_file, model_path, caffe.TEST)

    weights = dict()
    for key, val in net.params.items():
        print key,
        for i in range(len(val)):
            print val[i].data.shape,
        print

        weights[key] = [val[i].data for i in range(len(val))]

    np.savez(target_file, weights)
