from pathlib import Path
from types import SimpleNamespace

import click
import numpy as np

from stereo_tools.utils import pfm


class FileInfo(SimpleNamespace):
    path: Path
    method: str
    distance: float
    type: str
    resolution: str

    @staticmethod
    def from_path(path) -> 'FileInfo':
        METHOD = 0
        RESOLUTION = 1
        SCENE = 2
        DISTANCE = 3
        TYPE = 4

        parts = path.stem.split('_')
        return FileInfo(
            path=path,
            method=parts[METHOD],
            distance=float(parts[DISTANCE]),
            type=parts[TYPE],
            resolution=parts[RESOLUTION],
            scene=parts[SCENE]
        )


@click.command()
@click.argument('data_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
def run(data_dir: str) -> None:
    data_path = Path(data_dir)
    for file_ in data_path.rglob('*disp*.pfm'):
        info = FileInfo.from_path(file_)

        # prepare output path
        path = file_.parent
        filename = file_.name
        output_file = path / filename.replace('_disp', '_depth')

        # generate depth
        if info.resolution == 'hd720':
            # constants after rektification
            f = 668.8368530273438
            baseline = -119.915
        else:
            raise RuntimeError(f'Unsupported resolution: {info.resolution}')

        disp, _ = pfm.load_pfm(Path(file_))
        # ignore values without disparity
        disp[disp == 0] = np.inf

        depth_data = np.abs(f * baseline / disp)

        print('Generating: ', output_file)
        pfm.save_pfm(output_file, depth_data)


if __name__ == '__main__':
    run()
