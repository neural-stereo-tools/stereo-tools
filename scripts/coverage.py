import logging
from pathlib import Path
from types import SimpleNamespace

import click
import numpy as np

from stereo_tools.utils import pfm

logger = logging.getLogger(__name__)


class FileInfo(SimpleNamespace):
    path: Path
    method: str
    distance: float
    type: str
    resolution: str

    @staticmethod
    def from_path(path) -> 'FileInfo':
        METHOD = 0
        RESOLUTION = 1
        SCENE = 2
        DISTANCE = 3
        TYPE = 4

        parts = path.stem.split('_')
        return FileInfo(
            path=path,
            method=parts[METHOD],
            distance=float(parts[DISTANCE]),
            type=parts[TYPE],
            resolution=parts[RESOLUTION],
            scene=parts[SCENE]
        )


@click.command()
@click.argument('data_dir', type=click.Path(exists=True, dir_okay=True, file_okay=False))
def run(data_dir: str):
    path = Path(data_dir)

    # load data
    file_paths = list(path.rglob('*_disp.pfm'))
    files = map(FileInfo.from_path, file_paths)

    for info in files:
        disp_data, _ = np.abs(pfm.load_pfm(info.path))
        height, width = disp_data.shape

        total_pixels = height * width
        pixels = np.sum(disp_data > 0)
        coverage = pixels / total_pixels

        print(info.scene, info.method, info.distance, info.resolution, coverage, sep=', ')


if __name__ == '__main__':
    run()
