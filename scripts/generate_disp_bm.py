from pathlib import Path

import click
import numpy as np

from stereo_tools import block_matching as b
from stereo_tools import models
from stereo_tools.utils import pfm


@click.command()
@click.argument('data_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
@click.argument('output_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
def run(data_dir: str, output_dir: str) -> None:
    data_path = Path(data_dir)
    output_path = Path(output_dir)
    for left_image in data_path.rglob('*left*.webp'):
        right_image = Path(str(left_image).replace('left', 'right'))

        limg = models.Image.load(left_image)
        rimg = models.Image.load(right_image)
        image_data = np.concatenate([limg.data, rimg.data], axis=1)
        rect_image = models.StereoImage.from_image(data=image_data, image=limg)
        disp = b.get_disparity(rect_image)

        # prepare output path
        output_file = output_path / f'{left_image.stem}_sgbm.pfm'

        print('Generating: ', output_file)
        pfm.save_pfm(output_file, disp.data)


if __name__ == '__main__':
    run()
