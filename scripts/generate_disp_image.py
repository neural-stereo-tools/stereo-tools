from pathlib import Path

import click
import numpy as np
from scipy.misc import toimage

from stereo_tools.utils import pfm


@click.command()
@click.argument('min_disp', type=int)
@click.argument('max_disp', type=int)
@click.argument('data_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
def run(min_disp: int, max_disp: int, data_dir: str) -> None:
    data_path = Path(data_dir)
    for file_ in data_path.rglob('*.pfm'):
        # prepare output path
        path = file_.parent
        filename = file_.name
        output_file = path / f'{filename}.png'

        disp, _ = pfm.load_pfm(Path(file_))

        # normalize
        disp = np.abs(disp)
        disp -= disp.min()  # min_disp
        disp /= disp.max()  # max_disp
        disp *= 255

        print(output_file)
        toimage(disp).save(str(output_file))


if __name__ == '__main__':
    run()
