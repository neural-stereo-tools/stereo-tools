from pathlib import Path

import click
import cv2
import numpy as np
from stereo_tools.models import StereoImage, StereoCamera
from stereo_tools.calibrate import find_chessboard_corners
from stereo_tools.utils import click as types


def distort(point, cmat, dist):
    point = point[0]
    k1, k2, p1, p2, k3 = dist
    fx = cmat[0][0]
    fy = cmat[1][1]
    cx = cmat[0][2]
    cy = cmat[1][2]

    # To relative coordinates <- this is the step you are missing.
    # x = (point[0] - cx) / fx
    # y = (point[1] - cy) / fy
    x, y = point

    r2 = x * x + y * y
    # Radial distorsion
    xDistort = x * (1 + k1 * r2 + k2 * r2 * r2 + k3 * r2 * r2 * r2)
    yDistort = y * (1 + k1 * r2 + k2 * r2 * r2 + k3 * r2 * r2 * r2)
    # Tangential distorsion
    xDistort = xDistort + (2 * p1 * x * y + p2 * (r2 + 2 * x * x))
    yDistort = yDistort + (p1 * (r2 + 2 * y * y) + 2 * p2 * x * y)
    # Back to absolute coordinates.
    xDistort = xDistort * fx + cx
    yDistort = yDistort * fy + cy

    return np.asarray([xDistort, yDistort])


def ZED():
    print('ZED')
    M1 = np.asarray([
        [699.844970703125, 0, 648.2100219726562],
        [0, 699.844970703125, 360.3529968261719],
        [0, 0, 1],
    ])
    d1 = np.asarray([-0.17106199264526367, 0.026164300739765167, 0.0, 0.0, 0.0])
    M2 = np.asarray([
        [699.2849731445312, 0, 626.4359741210938],
        [0, 699.2849731445312, 356.12799072265625],
        [0, 0, 1],
    ])
    d2 = np.asarray([-0.1713699996471405, 0.02627049945294857, 0.0, 0.0, 0.0])
    T = np.asarray([119.91300201416016, 0.0, 0.0])
    R = cv2.Rodrigues(
        np.asarray([-0.00259634992107749, 0.014316299930214882, -0.00030825199792161584])
    )[0]
    return M1, d1, M2, d2, T, R


def MY():
    print('MY')
    M1 = np.asarray([
        [696.5275241926638, 0.0, 648.1248952165428],
        [0.0, 696.6552881548076, 362.87857835388155],
        [0.0, 0.0, 1.0],
    ])
    d1 = np.asarray([-0.15935323720805147, -0.00037625090546105966, 0.0, 0.0, 0.01457090169978159])  # NOQA
    M2 = np.asarray([
        [696.9523528017314, 0.0, 629.0116576942085],
        [0.0, 696.999798346865, 357.494776473274],
        [0.0, 0.0, 1.0],
    ])
    d2 = np.asarray([-0.16042939228500525, -0.0048425172360243955, 0.0, 0.0, 0.020852428599738647])  # NOQA
    T = np.asarray([-5.983900591647597, 0.007192576407150826, -0.03460473793256286])
    R = np.asarray([
        [0.9999310437838694, 2.149953259841032e-05, 0.011743390271614286],
        [-6.606127646578778e-05, 0.9999927996304117, 0.003794248705502994],
        [-0.01174322414029038, -0.0037947628518205834, 0.9999238453310778],
    ])
    return M1, d1, M2, d2, T, R


@click.command()
@click.argument('checkerboard', type=types.TupleIntType())
@click.argument('stereo_image', type=click.Path(exists=True, dir_okay=False))
def run(checkerboard, stereo_image: str):
    stereo_image = StereoImage.load(Path(stereo_image))

    for cam in (MY, ZED):
        M1, d1, M2, d2, T, R = cam()

        for image, (matrix, dist) in zip([stereo_image.left, stereo_image.right],
                                         [(M1, d1), (M2, d2)]):
            original_points = find_chessboard_corners(image, checkerboard)
            points = cv2.undistortPoints(original_points, matrix, dist)
            original_points = np.squeeze(original_points, axis=1)
            d_points = np.asarray([distort(p, matrix, dist)
                                   for p in points])

            error = np.sqrt(np.sum((original_points - d_points) ** 2))
            print(error.sum())
            print(error.mean())
            print('')


if __name__ == '__main__':
    run()
