import logging
from pathlib import Path
from types import SimpleNamespace
from typing import Any, Optional

import click
import numpy as np
import plyfile
import scipy.optimize
from plyfile import PlyData, PlyElement
from scipy.misc import imread

from stereo_tools.utils import pfm

logger = logging.getLogger(__name__)


class FileInfo(SimpleNamespace):
    path: Path
    method: str
    distance: float
    type: str
    resolution: str
    num: int = 1

    @staticmethod
    def from_path(path: Path) -> 'FileInfo':
        METHOD = 0
        RESOLUTION = 1
        SCENE = 2
        DISTANCE = 3
        TYPE = 4

        parts = path.stem.split('_')
        try:
            num = int(parts[-1])
        except Exception:
            num = 1

        return FileInfo(
            path=path,
            method=parts[METHOD],
            distance=float(parts[DISTANCE]),
            type=parts[TYPE],
            resolution=parts[RESOLUTION],
            scene=parts[SCENE],
            num=num,
        )


class PLYObject:
    def __init__(self, path):
        self.plydata = plyfile.PlyData.read(str(path))

    def get_vertices(self):
        return np.asarray([self.plydata['vertex'][dim] for dim in ('x', 'y', 'z')])

    def fit_sphere(self, radius=346.84):
        v = self.get_vertices()
        initial_c = np.mean(v, axis=1)
        initial_r = np.mean(
            np.sqrt(np.square(v[0] - initial_c[0]) +
                    np.square(v[1] - initial_c[1]) +
                    np.square(v[2] - initial_c[2]))
        )
        func = lambda arg: np.abs(np.sqrt(np.square(v[0] - arg[0]) +
                                          np.square(v[1] - arg[1]) +
                                          np.square(v[2] - arg[2])) -
                                  arg[3] + np.sqrt((radius - arg[3])**2))
        opt = scipy.optimize.leastsq(func, np.append(initial_c, [initial_r]))
        return tuple(opt[0]) + (func(opt[0]).mean(),)

    def fit_plane(self):
        v = self.get_vertices()
        for _ in range(50):
            pts = v[:, np.random.choice(v.shape[1], 3, replace=False)].T
            initial_a = np.cross(pts[0] - pts[2], pts[1] - pts[2])
            initial_d = np.inner(initial_a, pts[2])
            if initial_d != 0:
                break

        initial_a /= initial_d
        func = lambda arg: (np.abs(arg[0] * v[0] +
                                   arg[1] * v[1] +
                                   arg[2] * v[2] - 1) /
                            np.sqrt(arg[0] ** 2 + arg[1] ** 2 + arg[2] ** 2))
        opt = scipy.optimize.leastsq(func, initial_a)
        return tuple(opt[0]) + (func(opt[0]).mean(),)


class MaskInfo(SimpleNamespace):
    data: Any
    path: Path
    resolution: str
    scene: str

    @staticmethod
    def from_file(ref_file: FileInfo, dir: Path, suffix=None) -> Optional['MaskInfo']:
        basename = '_'.join([
            ref_file.scene,
            ref_file.resolution,
            str(ref_file.distance),
        ])
        if suffix:
            basename = f'{basename}_{suffix}'

        mask_path = dir / f'{basename}.png'
        if not mask_path.is_file():
            logger.warning(f'Could not find file: {mask_path}')
            return None
        img = imread(str(mask_path), mode='L')

        return MaskInfo(
            data=img > 0,
            path=mask_path,
            resolution=ref_file.resolution,
            scene=ref_file.scene,
            distance=ref_file.distance,
        )


@click.command()
@click.option('--resolution', type=str, default='all')
@click.option('--mask_suffix', type=str, default='')
@click.option('--scene',
              type=click.Choice(['whitewall', 'sphere', 'edge', 'all']), default='all')
@click.option('--fit',
              type=click.Choice(['plane', 'sphere']), default='plane')
@click.option('--output_dir',
              type=click.Path(exists=True, dir_okay=True, file_okay=False))
@click.argument('masks_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
@click.argument('data_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
def run(masks_dir: str, data_dir: str,
        mask_suffix: str,
        scene: str, resolution: str,
        fit: str,
        output_dir: str) -> None:
    masks_path = Path(masks_dir)
    data_path = Path(data_dir)

    # filters
    by_resolution = lambda fi: resolution == 'all' or fi.resolution == resolution
    by_scene = lambda fi: scene == 'all' or fi.scene == scene

    files = map(FileInfo.from_path, data_path.rglob('*depth*.pfm'))
    files = filter(by_resolution, files)
    files = filter(by_scene, files)

    for info in files:
        # generate depth
        if info.resolution == 'hd720':
            # constants after rektification
            f = 668.8368530273438
            cx = 626.5720825195312
            cy = 358.0090026855469
        else:
            raise RuntimeError(f'Unsupported resolution: {info.resolution}')

        mask = MaskInfo.from_file(info, masks_path, suffix=mask_suffix)
        if not mask:
            raise RuntimeError('No Mask found...')
            continue

        # generate PointCloud
        depth_data, _ = pfm.load_pfm(info.path)
        if info.method.lower().startswith('zed'):
            depth_data = np.flipud(depth_data)

        # exclude data without valid disp
        disp_mask = depth_data > 0
        data_mask = disp_mask & mask.data

        points = np.asarray(np.column_stack(np.ma.where(data_mask)))
        x = points[:, 1]
        y = points[:, 0]
        u = x - cx
        v = y - cy
        Z = depth_data[data_mask]
        X = u * Z / f
        Y = v * Z / f

        # generate .ply
        data = np.asarray([(x, y, z) for x, y, z in zip(X, Y, Z)],
                          dtype=[('x', 'f4'), ('y', 'f4'), ('z', 'f4')])
        el = PlyElement.describe(data, 'vertex')
        if output_dir:
            output_path = Path(output_dir)
            ply_file = output_path / info.path.name.replace('pfm', 'ply')
        else:
            ply_file = Path(str(info.path).replace('pfm', 'ply'))
        PlyData([el]).write(str(ply_file))

        # do fitting
        ply = PLYObject(str(ply_file))
        if fit == 'plane':
            *solution, error = ply.fit_plane()
        elif fit == 'sphere':
            *solution, error = ply.fit_sphere()
        else:
            raise RuntimeError(f'Unknown method for fitting: {fit}')

        print(info.scene, info.method, info.distance, info.resolution, info.num, error, sep=', ')  # NOQA


if __name__ == "__main__":
    run()
