from pathlib import Path

import click
import cv2
import numpy as np

from stereo_tools import models
from stereo_tools.calibrate import find_chessboard_corners
from stereo_tools.utils import click as types


def ZED():
    """
    Calibration data estimated by stereolab.
    """
    M1 = np.asarray([
        [699.844970703125, 0, 648.2100219726562],
        [0, 699.844970703125, 360.3529968261719],
        [0, 0, 1],
    ])
    d1 = np.asarray([-0.17106199264526367, 0.026164300739765167, 0.0, 0.0, 0.0])
    M2 = np.asarray([
        [699.2849731445312, 0, 626.4359741210938],
        [0, 699.2849731445312, 356.12799072265625],
        [0, 0, 1],
    ])
    d2 = np.asarray([-0.1713699996471405, 0.02627049945294857, 0.0, 0.0, 0.0])
    T = np.asarray([119.91300201416016, 0.0, 0.0])  # NOQA
    R = cv2.Rodrigues(
        np.asarray([-0.00259634992107749, 0.014316299930214882, -0.00030825199792161584])
    )[0]
    return M1, d1, M2, d2, T, R


def MY():
    """
    Calibration data estimated by openCV.
    """
    M1 = np.asarray([
        [696.5275241926638, 0.0, 648.1248952165428],
        [0.0, 696.6552881548076, 362.87857835388155],
        [0.0, 0.0, 1.0],
    ])
    d1 = np.asarray([-0.15935323720805147, -0.00037625090546105966, 0.0, 0.0, 0.01457090169978159])  # NOQA
    M2 = np.asarray([
        [696.9523528017314, 0.0, 629.0116576942085],
        [0.0, 696.999798346865, 357.494776473274],
        [0.0, 0.0, 1.0],
    ])
    d2 = np.asarray([-0.16042939228500525, -0.0048425172360243955, 0.0, 0.0, 0.020852428599738647])  # NOQA
    T = np.asarray([-5.983900591647597, 0.007192576407150826, -0.03460473793256286])
    R = np.asarray([
        [0.9999310437838694, 2.149953259841032e-05, 0.011743390271614286],
        [-6.606127646578778e-05, 0.9999927996304117, 0.003794248705502994],
        [-0.01174322414029038, -0.0037947628518205834, 0.9999238453310778],
    ])
    return M1, d1, M2, d2, T, R


def make_homogeneous(points):
    """
    Convert 2D points to homogeneous coordinates.
    """
    points = np.squeeze(points)
    if points.shape[1] != 3:
        return np.hstack((points, np.ones((points.shape[0], 1))))
    return points


def normalize_vector(line):
    n = np.sqrt((line[0]**2 + line[1]**2 + line[2]**2))
    return line / abs(n)


@click.command()
@click.argument('checkerboard', type=types.TupleIntType())
@click.argument('image_path', type=click.Path(exists=True, dir_okay=False))
def run(checkerboard,
        image_path: str) -> None:
    image = models.StereoImage.load(Path(image_path))

    for cam in (MY, ZED):
        print('\nCalibration data: ', cam.__name__)
        print('----------------------')
        M1, d1, M2, d2, T, R = cam()

        # get chessboard point
        left_points = find_chessboard_corners(image.left, checkerboard)
        right_points = find_chessboard_corners(image.right, checkerboard)
        # undistort them
        left_points = cv2.undistortPoints(left_points, M1, d1)
        right_points = cv2.undistortPoints(right_points, M2, d2)
        # prepare points for calculations
        left_points = make_homogeneous(left_points)
        right_points = make_homogeneous(right_points)

        # calc F
        S = np.asarray([
            [0, -T[2], T[1]],
            [T[2], 0, -T[0]],
            [-T[1], T[0], 0],
        ])
        F = np.linalg.inv(M1.transpose()) @ R @ S @ np.linalg.inv(M2)

        point_count = len(left_points)

        # Error using epipolar lines
        d_l = np.empty(point_count)
        d_r = np.empty(point_count)
        for i, (p_l, p_r) in enumerate(zip(left_points, right_points)):
            # get epipolar lines, l = (a, b, c)
            l_l = F.T.dot(p_r)
            l_r = F.dot(p_l)

            # calc distance between point and line
            # d = |ax + bx + c| / sqrt(a^2 + b^2)
            d_l[i] = np.abs(l_l.dot(p_l)) / np.sqrt(l_l[:2].dot(l_l[:2]))
            d_r[i] = np.abs(l_r.dot(p_r)) / np.sqrt(l_r[:2].dot(l_r[:2]))

        print('Epipolar line error')
        print('left:', d_l.sum(), d_l.mean())
        print('right', d_r.sum(), d_r.mean())
        print('Residual error: ', np.sum(d_l ** 2 + d_r ** 2) / point_count)

        # print('\nEpipolar geometry property')
        # err = np.empty(point_count)
        # for i, (p_l, p_r) in enumerate(zip(left_points, right_points)):
        #     err[i] = np.abs(p_l.T.dot(F).dot(p_r))
        # print(err.sum())
        # print(err.mean())


if __name__ == '__main__':
    run()
