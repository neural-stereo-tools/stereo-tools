import click
import numpy as np

from bokeh import palettes
from bokeh.layouts import gridplot
from bokeh.plotting import figure, show, output_file


@click.command()
@click.argument('csv_file', type=click.Path(exists=True, dir_okay=False))
@click.argument('output', type=click.Path(exists=False))
def run(csv_file, output):
    data = np.genfromtxt(csv_file, delimiter=',', names=True)

    TOOLS = 'pan,wheel_zoom,box_zoom,reset,save,box_select'
    colors = iter(palettes.Paired[12])
    x = data['epoch']

    p1 = figure(title="Accuracy", tools=TOOLS)
    for name, color in zip(['val_acc', 'acc'], colors):
        p1.line(x, data[name], legend=name, color=color)

    p2 = figure(title="Average error", tools=TOOLS)
    for name, color in zip(['val_avgerr', 'avgerr'], colors):
        p2.line(x, data[name], legend=name, color=color)

    p3 = figure(title="Loss", tools=TOOLS)
    for name, color in zip(['val_loss', 'loss'], colors):
        p3.line(x, data[name], legend=name, color=color)

    output_file(output, title="Training history")
    show(gridplot([[p1, p2, p3]]))


if __name__ == '__main__':
    run()
