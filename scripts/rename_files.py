import shutil
from pathlib import Path

import click


@click.command()
@click.argument('data_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
@click.argument('output_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
def run(data_dir: str, output_dir: str, series: bool = True):
    SCENE = 0
    DISTANCE = 1
    RESOLUTION = 2
    NUM = 3
    METHOD = -1

    data_path = Path(data_dir)
    output_path = Path(output_dir)
    for file_ in data_path.rglob('*.pfm'):
        basename = file_.stem
        # normalize
        basename = basename.replace('white_wall', 'whitewall')
        basename = basename.replace('spere', 'sphere')

        extension = file_.suffix
        parts = basename.split('_')
        nbasename = '_'.join([
            parts[METHOD],
            parts[RESOLUTION],
            parts[SCENE],
            parts[DISTANCE],
            'disp',
            parts[NUM],
        ])
        nfilename = f'{nbasename}{extension}'
        target_file = output_path / nfilename

        print(file_, '->', target_file)
        shutil.copyfile(file_, target_file)


@click.command()
@click.argument('data_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
@click.argument('output_dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False))
def run_zed(data_dir: str, output_dir: str, series: bool = True):
    if series:
        FILL_MODE = -2
        SENSING_MODE = -3
        RESOLUTION = -4
        DISTANCE = -5
        SCENE = -6
    else:
        FILL_MODE = -1
        SENSING_MODE = -2
        RESOLUTION = -3
        DISTANCE = -4
        SCENE = -5

    data_path = Path(data_dir)
    output_path = Path(output_dir)
    for file_ in data_path.rglob('*.pfm'):
        type_ = file_.stem.replace('disparity', 'disp')

        # normalize
        path = str(file_.parent)
        path = path.replace('white_wall', 'whitewall')
        path = path.replace('spere', 'sphere')

        parts = path.split('/')

        if series:
            extension = file_.name
            nbasename = '_'.join([
                parts[SCENE],
                parts[DISTANCE],
                parts[RESOLUTION],
                '',
            ])

            nbasename += '{}_'.format(parts[-1])
        else:
            # get method
            method = '-'.join([
                'ZED',
                parts[SENSING_MODE],
                parts[FILL_MODE],
            ])

            extension = file_.suffix
            nbasename = '_'.join([
                method,
                parts[RESOLUTION],
                parts[SCENE],
                parts[DISTANCE],
                type_,
            ])

        nfilename = f'{nbasename}{extension}'
        target_file = output_path / nfilename

        print(file_, '->', target_file)
        shutil.copyfile(file_, target_file)


if __name__ == '__main__':
    run()
