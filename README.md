# Neural Stereo Tools

Depth Map Reconstruction using Neural Networks. Available Networks are
DispNet and DispNetC.

## Requirements

* Python 3.x
* imagemagic

## Install


        python -m venv _venv
        _venv/bin/pip install -U pip
        _venv/bin/pip install -e ".[dev]"


## Scripts

After install, there are multiple scripts available.
See `--help` for usage information:

* calibrate - Calculate f-mat and distortion parameters
* block_matching - Disparity map reconstruction using SGBM
* depth_map - create a depth map from disparity map
* pointcloud - create a point cloud from .pfm file
* predict - Disparity map reconstruction using neural networks
* train - Train a neural network
