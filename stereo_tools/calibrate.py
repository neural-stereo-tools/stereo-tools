import logging
from typing import Iterable, Tuple

import cv2
import numpy as np

from . import models

logger = logging.getLogger(__name__)


CRITERIA = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER,
            30, 0.1)


class CalibrationError(RuntimeError):
    pass


def find_chessboard_corners(imgc: models.Image,
                            pattern_size: Tuple[int, int],
                            draw: bool = False) -> np.ndarray:
    """
    Finds checkerboard on the given image and returns the coordinates of each
    crossing as list.
    """
    img = imgc.grayscale()
    found, corners = cv2.findChessboardCorners(img.data, pattern_size)

    if found:
        cv2.cornerSubPix(img.data, corners, (5, 5), (-1, -1),
                         CRITERIA)
    else:
        raise ValueError(
            f'Could not find chessboard on {img.filename}.'
        )

    if draw:
        # Draw and display the corners
        cv2.drawChessboardCorners(imgc.data, pattern_size, corners, True)
        imgc.show()
        input()
    return corners


def get_intrinsic_parameters(img_points,
                             obj_points,
                             img_size: Tuple[int, int]) -> models.CameraParameters:
    """
    Calcualtes the intrinsic camera parameters for the given
    checkerboard points.
    """
    # calculate camera distortion
    ret, camera_matrix, dist_coefs, _, _ = cv2.calibrateCamera(
        obj_points, img_points, img_size, None, None)

    if not ret:
        raise CalibrationError(
            'Could not calculate intrinsic parameters.'
        )

    return models.CameraParameters(
        matrix=camera_matrix,
        distortion=dist_coefs,
    )


def get_extrinsic_parameters(initial_camera_left: models.CameraParameters,
                             initial_camera_right: models.CameraParameters,
                             obj_points,
                             img_points_left,
                             img_points_right,
                             img_size: Tuple[int, int]) -> models.StereoCamera:
    """
    Calculates the extrinsic parameters, based on the calibrated cameras
    and the given image points.
    """
    (ret,
     camera_matrix_left, dist_coefs_left,
     camera_matrix_right, dist_coefs_right,
     rotation_matix, translation_matrix,
     essential_matrix, fundamental_matrix) = cv2.stereoCalibrate(
        obj_points, img_points_left, img_points_right,
        initial_camera_left.matrix, initial_camera_left.distortion,
        initial_camera_right.matrix, initial_camera_right.distortion,
        img_size,
        flags=(
            cv2.CALIB_USE_INTRINSIC_GUESS +  # optimize intrinsic params
            cv2.CALIB_FIX_ASPECT_RATIO +  # optimize f_y
            cv2.CALIB_FIX_FOCAL_LENGTH +  # optimize f_x
            cv2.CALIB_ZERO_TANGENT_DIST +
            cv2.CALIB_FIX_K1 +
            cv2.CALIB_FIX_K2 +
            cv2.CALIB_FIX_K3
        ),
        criteria=CRITERIA)

    if not ret:
        raise CalibrationError(
            'Could not calculate extrinsic parameters.'
        )

    left_camera = models.CameraParameters(
        matrix=camera_matrix_left, distortion=dist_coefs_left)
    right_camera = models.CameraParameters(
        matrix=camera_matrix_right, distortion=dist_coefs_right)

    return models.StereoCamera(
        left=left_camera,
        right=right_camera,
        height=img_size[0],
        width=img_size[1],
        rotation_matrix=rotation_matix,
        translation_matrix=translation_matrix,
        essential_matrix=essential_matrix,
        fundamental_matrix=fundamental_matrix,
    )


def calibrate(images: Iterable[models.StereoImage],
              pattern_size: Tuple[int, int]) -> models.StereoCamera:
    """
    Calculates the intrinsics of each camera and the extrinsics of the stereo
    camera.
    """
    img_points_left = []  # 2d points in image plane
    img_points_right = []  # 2d points in image plane

    img = None
    for img in images:
        print('Detecting checkerboard on `{}`'.format(img.filename))
        try:
            corners_left = find_chessboard_corners(img.left, pattern_size)
            corners_right = find_chessboard_corners(img.right, pattern_size)
        except ValueError:
            logger.warning(
                'Could not find checkerboard on {} or {}.'.format(
                    img.left.filename,
                    img.right.filename,
                )
            )
            continue
        else:
            img_points_left.append(corners_left)
            img_points_right.append(corners_right)

    if img is None:
        raise CalibrationError(
            'Could not load images.'
        )

    # use size of last image
    img_size = (img.height, img.width)

    # prepare object points of one checkerboard,
    # like (0,0,0), (1,0,0), (2,0,0) ..,(6,5,0)
    pattern_points = np.zeros((np.prod(pattern_size), 3), np.float32)
    pattern_points[:, :2] = np.indices(pattern_size).T.reshape(-1, 2)

    # we need object points for each image
    # it represents a 3d point in real world space
    obj_points = [pattern_points for _ in range(len(img_points_left))]

    # calibrate each camera
    print('Calculate intrinsics')
    camera_left = get_intrinsic_parameters(
        img_points_left, obj_points, img_size)
    camera_right = get_intrinsic_parameters(
        img_points_right, obj_points, img_size)

    # calibrate the stereo camera
    print('Calculate extrinsics')
    return get_extrinsic_parameters(
        camera_left, camera_right,
        obj_points,
        img_points_left, img_points_right,
        img_size,
    )
