import keras.backend as K
from keras.layers import Lambda, Input, ZeroPadding2D, multiply
from keras.models import Model

from .dispnet import conv_cell, DispNet, upconv_cell


def correlation(channels, *, max_disp=40):
    def inner(conva, convb):
        shape = K.shape(conva)

        pconvb = ZeroPadding2D(padding=((0, 0), (max_disp, max_disp)))(convb)
        costs = []
        for d in range(2 * max_disp + 1):
            slice_b = Lambda(
                lambda x: x[:, :, d:d + shape[2], :],
            )(pconvb)
            vecs = multiply([conva, slice_b])
            cost = Lambda(
                lambda x: K.sum(x, axis=-1) / channels  # / shape[-1]
            )(vecs)
            costs.append(cost)
        return Lambda(
            lambda x: K.stack(x, axis=-1),
            # output_shape=(shape[1], shape[2], 2 * max_disp),
        )(costs)
    return inner


def shared_conv_cell(input_shape, output, kernel, strides, activation='relu',
                     name=None):
    from keras.layers import Conv2D
    from keras.layers.advanced_activations import LeakyReLU

    return Conv2D(output, kernel_size=kernel, strides=strides,
                  activation=LeakyReLU(alpha=0.1), use_bias=True,
                  padding='same', name=name, input_shape=input_shape)


def dispnet_block(left_tensor, right_tensor):
    conv1 = shared_conv_cell(left_tensor._keras_shape[:1], 64, 7, 2, name='conv1')
    conv1a = conv1(left_tensor)
    conv1b = conv1(right_tensor)

    conv2 = shared_conv_cell(conv1a._keras_shape[:1], 128, 5, 2, name='conv2')
    conv2a = conv2(conv1a)
    conv2b = conv2(conv1b)

    conv_redir = conv_cell(256, 0, 1, 1, name='conv_redir')(conv2a)

    corr = correlation(
        128,
        max_disp=40,
    )(conv2a, conv2b)

    blob16 = Lambda(
        lambda x: K.concatenate(x, axis=-1),
        # output_shape=(shape1[1], shape1[2], shape1[3] + shape2[3])
    )([corr, conv_redir])

    conv3a = conv_cell(256, 2, 5, 2, name='conv3')(blob16)
    conv3b = conv_cell(256, 1, 3, 1, name='conv3_1')(conv3a)
    conv4a = conv_cell(512, 1, 3, 2, name='conv4')(conv3b)
    conv4b = conv_cell(512, 1, 3, 1, name='conv4_1')(conv4a)
    conv5a = conv_cell(512, 1, 3, 2, name='conv5')(conv4b)
    conv5b = conv_cell(512, 1, 3, 1, name='conv5_1')(conv5a)
    conv6a = conv_cell(1024, 1, 3, 2, name='conv6')(conv5b)
    conv6b = conv_cell(1024, 1, 3, 1, name='conv6_1')(conv6a)

    concat5 = upconv_cell(
        512,
        predict_name='Convolution1',
        upsample_name='upsample_flow6to5',
        deconv_name='deconv5',
        conv_name='Convolution2',
    )(conv5b, conv6b)
    concat4 = upconv_cell(
        256,
        predict_name='Convolution3',
        upsample_name='upsample_flow5to4',
        deconv_name='deconv4',
        conv_name='Convolution4',
    )(conv4b, concat5)
    concat3 = upconv_cell(
        128,
        predict_name='Convolution5',
        upsample_name='upsample_flow4to3',
        deconv_name='deconv3',
        conv_name='Convolution6',
    )(conv3b, concat4)
    concat2 = upconv_cell(
        64,
        predict_name='Convolution7',
        upsample_name='upsample_flow3to2',
        deconv_name='deconv2',
        conv_name='Convolution8',
    )(conv2a, concat3)
    concat1 = upconv_cell(
        32,
        predict_name='Convolution9',
        upsample_name='upsample_flow2to1',
        deconv_name='deconv1',
        conv_name='Convolution10',
    )(conv1a, concat2)

    prediction = conv_cell(1, 1, 3, 1, activation=None,
                           name='Convolution11')(concat1)

    return prediction


class DispNetC(DispNet):
    def get_model(self):
        left_input = Input(shape=(self.height, self.width, 3))
        right_input = Input(shape=(self.height, self.width, 3))

        processed_left_input = self.preprocess(left_input)
        processed_right_input = self.preprocess(right_input)

        # run dispnet
        disp_tensor = dispnet_block(processed_left_input,
                                    processed_right_input)
        disp = self.postprocess(disp_tensor)

        model = Model(
            inputs=[left_input, right_input],
            outputs=[disp],
        )
        return model
