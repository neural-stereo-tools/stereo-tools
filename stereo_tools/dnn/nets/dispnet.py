import logging
import math
from pathlib import Path

import keras.backend as K
import tensorflow as tf
from keras.callbacks import LearningRateScheduler
from keras.layers import Conv2D, Conv2DTranspose, Cropping2D, Input, Lambda, ZeroPadding2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.merge import concatenate
from keras.models import Model
from keras.optimizers import Adam

from .base import Network

logger = logging.getLogger(__name__)


def conv_cell(output, padding, kernel, strides, activation='relu',
              name=None):
    def inner(input_layer):
        pad = ZeroPadding2D(padding=padding)(input_layer)
        conv = Conv2D(output, kernel_size=kernel, strides=strides,
                      activation=None, use_bias=True,
                      name=name)(pad)
        if activation == 'relu':
            # always use a leaky ReLU
            conv = LeakyReLU(alpha=0.1)(conv)
        return conv
    return inner


def deconv_cell(output, padding, kernel, strides, activation='relu',
                name=None):
    def inner(input_layer):
        deconv = Conv2DTranspose(
            output, kernel_size=kernel, strides=strides,
            activation=None, use_bias=True,
            name=name)(input_layer)
        if activation == 'relu':
            deconv = LeakyReLU(alpha=0.1)(deconv)
        crop = Cropping2D(1)
        return crop(deconv)
    return inner


def upconv_cell(output,
                deconv_name=None, predict_name=None,
                upsample_name=None, conv_name=None):
    def inner(input1, input2):
        predict_flow = conv_cell(1, 1, 3, 1, activation=None,
                                 name=predict_name)(input2)
        upsample = deconv_cell(1, 1, 4, 2, activation=None,
                               name=upsample_name)(predict_flow)

        deconv = deconv_cell(output, 1, 4, 2,
                             name=deconv_name)(input2)

        merge = concatenate([input1, deconv, upsample])

        conv = conv_cell(output, 1, 3, 1, activation=None,
                         name=conv_name)
        return conv(merge)
    return inner


def dispnet_block(input_tensor):
    conv1a = conv_cell(64, 3, 7, 2, name='conv1')(input_tensor)
    conv2a = conv_cell(128, 2, 5, 2, name='conv2')(conv1a)
    conv3a = conv_cell(256, 2, 5, 2, name='conv3')(conv2a)
    conv3b = conv_cell(256, 1, 3, 1, name='conv3_1')(conv3a)
    conv4a = conv_cell(512, 1, 3, 2, name='conv4')(conv3b)
    conv4b = conv_cell(512, 1, 3, 1, name='conv4_1')(conv4a)
    conv5a = conv_cell(512, 1, 3, 2, name='conv5')(conv4b)
    conv5b = conv_cell(512, 1, 3, 1, name='conv5_1')(conv5a)
    conv6a = conv_cell(1024, 1, 3, 2, name='conv6')(conv5b)
    conv6b = conv_cell(1024, 1, 3, 1, name='conv6_1')(conv6a)

    concat5 = upconv_cell(
        512,
        predict_name='Convolution1',
        upsample_name='upsample_flow6to5',
        deconv_name='deconv5',
        conv_name='Convolution2',
    )(conv5b, conv6b)
    concat4 = upconv_cell(
        256,
        predict_name='Convolution3',
        upsample_name='upsample_flow5to4',
        deconv_name='deconv4',
        conv_name='Convolution4',
    )(conv4b, concat5)
    concat3 = upconv_cell(
        128,
        predict_name='Convolution5',
        upsample_name='upsample_flow4to3',
        deconv_name='deconv3',
        conv_name='Convolution6',
    )(conv3b, concat4)
    concat2 = upconv_cell(
        64,
        predict_name='Convolution7',
        upsample_name='upsample_flow3to2',
        deconv_name='deconv2',
        conv_name='Convolution8',
    )(conv2a, concat3)
    concat1 = upconv_cell(
        32,
        predict_name='Convolution9',
        upsample_name='upsample_flow2to1',
        deconv_name='deconv1',
        conv_name='Convolution10',
    )(conv1a, concat2)

    prediction = conv_cell(1, 1, 3, 1, activation=None,
                           name='Convolution11')(concat1)

    return prediction


LEARNING_RATE = 0.0001


def rgb_to_bgr(tensor, axis=-1):
    red, green, blue = tf.unstack(tensor, axis=axis)
    return tf.stack([blue, green, red], axis=axis)


class DispNet(Network):
    def __init__(self, *args, **kwargs) -> None:
        self.caffe = kwargs.pop('caffe', True)
        super().__init__(*args, **kwargs)

        # rescale
        divisor = 64
        self.adapted_height = math.ceil(self.height / divisor) * divisor
        self.adapted_width = math.ceil(self.width / divisor) * divisor
        self.rescale_coefficient = self.width / self.adapted_width

        logger.debug(
            f'Rescale image from ({self.height}, {self.width}) to '
            f'({self.adapted_height}, {self.adapted_width})'
        )

    def get_callbacks(self,
                      initial_epoch: int,
                      batch_size: int,
                      iterations_per_epoch: int,
                      output_dir: Path,
                      validation: bool = False):
        # decay lr
        def scheduler(epoch):
            """
            Divides learning rate (lr) by 2 every 200k iterations,
            starting from iteration 400k.
            """
            # store current epoch
            scheduler.epoch = epoch

            MIN_ITERATIONS = 400000
            DECAY_ITERATIONS = 200000

            iterations = epoch * iterations_per_epoch * batch_size
            if iterations > MIN_ITERATIONS:
                divisor = 2 * math.ceil(
                    (iterations - MIN_ITERATIONS) / DECAY_ITERATIONS
                )
                K.set_value(self._model.optimizer.lr, LEARNING_RATE / divisor)

                print("lr changed to {}".format(LEARNING_RATE / divisor))

            return K.get_value(self._model.optimizer.lr)
        scheduler.epoch = initial_epoch - 1  # set initial epoch

        lr_decay = LearningRateScheduler(scheduler)

        callbacks = super().get_callbacks(initial_epoch,
                                          batch_size,
                                          iterations_per_epoch,
                                          output_dir,
                                          validation=validation)
        return callbacks + [lr_decay]

    def get_optimizer(self):
        return Adam(
            lr=LEARNING_RATE,
            beta_1=0.9,
            beta_2=0.999,
            epsilon=1e-08,  # value is unknown from paper, use default for now
            # divided by 2 every 200k iterations, starting from iteration 400k
            # NOTE: see `def train` for implementation
            decay=0.0,
        )

    def preprocess(self, tensor):
        if self.caffe:
            # convert to bgr
            tensor = Lambda(
                lambda x: rgb_to_bgr(x),
                output_shape=(self.height, self.width, 3),
            )(tensor)

        # convert numbers between [0;1]
        normalized_tensor = Lambda(
            lambda x: x * 1 / 255.,
            output_shape=(self.height, self.width, 3),
        )(tensor)

        def mean_per_channel(x):
            c0 = K.mean(x[:, :, :, 0])
            c1 = K.mean(x[:, :, :, 1])
            c2 = K.mean(x[:, :, :, 2])
            return K.stack([c0, c1, c2])

        # nomean
        nomean_tensor = Lambda(
            lambda x: x - mean_per_channel(x),
            output_shape=(self.height, self.width, 3),
        )(normalized_tensor)

        # rescale image for network
        scaled_tensor = Lambda(
            lambda x: tf.image.resize_bilinear(x, (self.adapted_height,
                                                   self.adapted_width)),
            output_shape=(self.adapted_height, self.adapted_width, 3),
        )(nomean_tensor)

        return scaled_tensor

    def postprocess(self, tensor):
        # resize to full resolution
        disp = Lambda(
            lambda x: tf.image.resize_bilinear(x, (self.height, self.width)),
            output_shape=(self.height, self.width, 1),
        )(tensor)

        # rescale disparity
        disp = Lambda(
            lambda x: x * self.rescale_coefficient,
            output_shape=(self.height, self.width, 1),
        )(disp)

        # squeeze last dimension
        disp = Lambda(
            lambda x: K.squeeze(x, axis=-1),
            output_shape=(self.height, self.width),
        )(disp)

        if self.caffe:
            # the original DispNet produces negative disparities
            # this is corrected by the Eltwise layer
            # we have to do this explicitly
            disp = Lambda(
                lambda x: x * -1,
                output_shape=(self.height, self.width),
            )(disp)

        return disp

    def get_model(self):
        left_input = Input(shape=(self.height, self.width, 3))
        right_input = Input(shape=(self.height, self.width, 3))

        processed_left_input = self.preprocess(left_input)
        processed_right_input = self.preprocess(right_input)

        # stack images for dispnet
        input_tensor = Lambda(
            lambda x: K.concatenate(x, axis=3),
            output_shape=(self.adapted_height, self.adapted_width, 6)
        )([processed_left_input, processed_right_input])

        # run dispnet
        disp_tensor = dispnet_block(input_tensor)
        disp = self.postprocess(disp_tensor)

        model = Model(inputs=[left_input, right_input], outputs=disp)
        return model
