import abc
import logging
from functools import partial
from pathlib import Path
from typing import Any, Iterable, Optional

import numpy as np
from keras.callbacks import CSVLogger, ModelCheckpoint

from .utils import avgerr, bad

logger = logging.getLogger(__name__)


class Network(abc.ABC):
    def __init__(self, *,
                 height: Optional[int] = None,
                 width: Optional[int] = None,
                 weights_path: Optional[Path] = None) -> None:
        self.height = height
        self.width = width
        self.weights_path = weights_path

    @abc.abstractmethod
    def get_model(self):
        ...

    @abc.abstractmethod
    def get_optimizer(self):
        ...

    def get_loss(self) -> Any:
        return 'mean_absolute_error'

    def get_metrics(self) -> Iterable[Any]:
        bad05 = partial(bad, 0.5)
        bad05.__name__ = 'bad05'
        bad1 = partial(bad, 1)
        bad1.__name__ = 'bad1'
        bad2 = partial(bad, 2)
        bad2.__name__ = 'bad2'
        bad4 = partial(bad, 4)
        bad4.__name__ = 'bad4'

        return ['accuracy', bad05, bad1, bad2, bad4, avgerr]

    def get_callbacks(self,
                      initial_epoch: int,
                      batch_size: int,
                      iterations_per_epoch: int,
                      output_dir: Path,
                      validation: bool = False):
        # model checkpoint
        if not validation:
            checkpont_filename = 'weights_epoch{epoch:02d}.hdf5'
        else:
            checkpont_filename = 'weights_epoch{epoch:02d}_loss{val_loss:.2f}.hdf5'

        model_checkpoint = ModelCheckpoint(
            str(output_dir / checkpont_filename),
            save_weights_only=True,
            period=2,
        )
        csv_logger = CSVLogger(
            str(output_dir / 'training.csv'),
            append=True,
        )
        return [model_checkpoint, csv_logger]

    def build(self):
        self._model = self.get_model()
        self._model.compile(
            loss=self.get_loss(),
            optimizer=self.get_optimizer(),
            metrics=self.get_metrics(),
        )
        if self.weights_path:
            logger.debug(f'Loading weights: {self.weights_path}')
            self._model.load_weights(str(self.weights_path))

        return self._model

    def predict(self, left_img, right_img):
        if not getattr(self, '_model', None):
            raise RuntimeError("Please call `.build()` first.")

        disp = self._model.predict(
            [np.asarray([left_img]), np.asarray([right_img])],
            batch_size=1,
        )

        # remove batch dimension
        disp = np.squeeze(disp)
        return disp

    def train(self,
              image_generator,
              batch_size: int,
              iterations_per_epoch: int,
              output_dir: Path,
              epochs=1, initial_epoch=1):
        if not getattr(self, '_model', None):
            raise RuntimeError("Please call `.build()` first.")

        # stop training on ctrl+c
        def signal_handler(signal, frame):
            self._model.stop_training = True
            print('\n!!! Training will be stopped after current epoch!\n')
        import signal
        signal.signal(signal.SIGINT, signal_handler)

        print('\nPress Ctrl+C to stop training after the current epoch.\n'
              'Your model will be saved to the current directory.\n\n')

        callbacks = self.get_callbacks(
            initial_epoch,
            batch_size,
            iterations_per_epoch,
            output_dir,
        )

        fit_args = (image_generator, iterations_per_epoch)
        fit_kwargs = {
            'epochs': epochs,
            'initial_epoch': initial_epoch,
            'callbacks': callbacks,
            'verbose': 1,
        }

        # fit the network
        self._model.fit_generator(*fit_args, **fit_kwargs)

    def save(self, output_dir) -> None:
        self._model.save_weights(str(output_dir / 'weight.hdf5'))
        try:
            self._model.save(str(output_dir / 'model.hdf5'))
        except Exception as e:
            print('Could not save model:', e)
