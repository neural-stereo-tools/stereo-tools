import random
from typing import List, Optional, Tuple

import keras.backend as K
import numpy as np

from stereo_tools.dnn.models import LazyStereoDispImage, StereoDispImage


def avgerr(y_true, y_pred):
    errors = K.abs(y_true - y_pred)
    return K.mean(errors)


def bad(threshold, y_true, y_pred):
    errors = K.abs(y_true - y_pred)
    # numbers of errors in current batch
    nb_errors = K.sum(
        K.cast(
            K.greater_equal(errors, threshold),
            dtype='int32'
        )
    )
    # number of pixels per image
    nb_pixels = K.shape(y_true)[1] * K.shape(y_true)[2]
    # number of pixes in current batch
    nb_pixels = K.shape(errors)[0] * nb_pixels

    return nb_errors / nb_pixels


def augment_image(image: StereoDispImage):
    from imgaug import augmenters as iaa
    if not hasattr(augment_image, 'indiviual'):
        indiviual_filters = [
            # fake minor vertical misscalibration
            iaa.Affine(translate_px={"y": (-1, 1)}),
            # change brightness of images (50-150% of original value)
            iaa.Multiply((0.5, 1.5)),
            # blur images with a sigma between 0 and 0.5
            iaa.GaussianBlur((0, 0.5)),
        ]
        aug = iaa.SomeOf((0, len(indiviual_filters)), indiviual_filters)
        aug.to_deterministic()
        augment_image.indiviual = aug

    if not hasattr(augment_image, 'general'):
        filters = [
            # change brightness of images (by -10 to 10 of original value)
            iaa.Add((-10, 10), per_channel=0.5),
            # improve or worsen the contrast/color
            iaa.ContrastNormalization((0.5, 1.5), per_channel=0.5),
            # add gaussian noise to images
            iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.03 * 255)),
        ]
        aug = iaa.SomeOf((0, len(filters)), filters)
        aug.to_deterministic()
        augment_image.general = aug

    # apply autmentation to both images
    img = augment_image.general.augment_image(image.data)

    # apply augenmation to each image individually
    height, width, _ = img.shape
    left = img[0:height, 0:int(width / 2)]
    right = img[0:height, int(width / 2):width]
    left = augment_image.indiviual.augment_image(left)
    right = augment_image.indiviual.augment_image(right)

    nimage = StereoDispImage.from_image(
        data=np.concatenate([left, right], axis=1),
        image=image,
        disp=image.disp,
    )
    return nimage


def infinite_training_gen(images: List[LazyStereoDispImage],
                          batch_size,
                          crop: Optional[Tuple[int, int]] = None,
                          augmentation: bool = True,
                          random_size: bool = False):
    """
    Generates chunks of training data with size `batch_size`. Returns a tuple
    with `batch_size` input and output data.
    """
    batch_crop = None

    # load images on the fly and cycle them forever
    images_iter = iter(images)
    while True:
        input_data = list()
        expected_data = list()

        if crop:
            if random_size:
                batch_crop = (random.randint(256, crop[0]),
                              random.randint(256, crop[1]))
            else:
                batch_crop = crop

        for _ in range(batch_size):
            # lazy load image infinitly
            try:
                limage = next(images_iter)
            except StopIteration:
                images_iter = iter(images)
                limage = next(images_iter)
            finally:
                image = limage.load()

            # in TF or numpy or scipy an image is (height, width, colors)
            if batch_crop:
                image = image.random_crop(*batch_crop)

            if augmentation:
                image = augment_image(image)

            input_data.append(image)
            expected_data.append(image.disp.data)

        yield (
            [
                np.asarray([d.left.data for d in input_data]),
                np.asarray([d.right.data for d in input_data]),
            ],
            np.asarray(expected_data),
        )
