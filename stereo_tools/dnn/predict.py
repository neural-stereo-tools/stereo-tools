from pathlib import Path

import scipy.misc

from .nets.base import Network


def get_network(network_name: str) -> Network:
    if network_name == 'dispnet':
        from .nets.dispnet import DispNet
        return DispNet
    elif network_name == 'dispnetc':
        from .nets.dispnetc import DispNetC
        return DispNetC
    elif network_name == 'gcnet':
        from .nets.gcnet import GCNet
        return GCNet
    else:
        raise ValueError(f"Network `{network_name}` not found.")


def predict(net: str, weights_path: Path, left_img_path: Path, right_img_path: Path):
    left_img = scipy.misc.imread(left_img_path)
    right_img = scipy.misc.imread(right_img_path)
    if left_img.shape != right_img.shape:
        raise ValueError(
            f"Shape of images must be equal. Got {left_img.shape} != {right_img.shape}"
        )

    height, width, _ = left_img.shape

    # construct network
    network_class = get_network(net)
    network = network_class(
        height=height, width=width,
        weights_path=weights_path,
    )
    network.build()
    return network.predict(left_img, right_img)
