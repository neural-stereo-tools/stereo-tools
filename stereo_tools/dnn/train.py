from pathlib import Path
from typing import Iterable, Optional, Tuple

from .datasets import ImageCollector
from .nets import utils
from .predict import get_network


def limit_mem():
    import keras.backend as K

    K.get_session().close()
    cfg = K.tf.ConfigProto()
    cfg.gpu_options.allow_growth = True
    K.set_session(K.tf.Session(config=cfg))


def train(network_name: str,
          datasets: Iterable[Path],
          output_dir: Path,
          *,
          weights_path: Optional[Path] = None,
          batch_size: int = 1,
          epochs: int = 2,
          initial_epoch: int = 1,
          crop: Optional[Tuple[int, int]] = None,
          random_size: bool = False,
          augmentation: bool = False,
          max_epoch_images: int = 2000):

        limit_mem()

        # load network
        network_class = get_network(network_name)
        network = network_class(
            height=crop[0] if crop else None,
            width=crop[1] if crop else None,
            weights_path=weights_path,
        )
        network.build()

        # load images
        images = []
        for dataset in datasets:
            images += list(ImageCollector.load_images(dataset))

        if not images:
            raise RuntimeError('No images loaded')
        print(f'Found {len(images)} images')

        # generate image generator
        image_generator = utils.infinite_training_gen(
            images, batch_size=batch_size,
            crop=crop, random_size=random_size,
            augmentation=augmentation,
        )

        # how many iterations per epoch?
        nb_images_per_epoch = max_epoch_images or 2000
        nb_training = len(images)
        if nb_training < batch_size and crop:
            iterations_per_epoch = batch_size
        else:
            iterations_per_epoch = int(
                min(nb_images_per_epoch, nb_training) / batch_size
            )

        if not iterations_per_epoch:
            RuntimeError('No iterations for training? O.o')

        # run training
        network.train(
            image_generator,
            batch_size,
            iterations_per_epoch,
            output_dir,
            epochs=epochs,
            initial_epoch=initial_epoch,
        )
        network.save(output_dir)
