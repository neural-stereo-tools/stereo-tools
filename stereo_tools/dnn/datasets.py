import abc
import logging
import re
from pathlib import Path
from typing import Generator, Optional

from stereo_tools.dnn.models import LazyStereoDispImage

logger = logging.getLogger(__name__)


class ImageCollector(abc.ABC):
    collectors = []
    extensions = []

    def __init__(self, path: Path) -> None:
        self.path = path
        logger.info(f'Loading images using {self} from {self.path}')

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.collectors.append(cls)

    @classmethod
    def load_images(cls, path: Path):
        for collector_class in cls.collectors:
            yield from collector_class(path)

    def __iter__(self) -> Generator[LazyStereoDispImage, None, None]:
        yield from self.collect_images()

    def _find_images(self) -> Generator[Path, None, None]:
        for ext in self.extensions:
            for img in self.path.glob(f'**/left/**/*.{ext}'):
                yield img

    def collect_images(self) -> Generator[LazyStereoDispImage, None, None]:
        for left_img_path in self._find_images():
            right_img_path = self.right_path_from_img(left_img_path)
            if not right_img_path.exists():
                continue

            disp_img_path = self.disp_path_from_img(left_img_path)
            if not disp_img_path or not disp_img_path.exists():
                continue

            yield LazyStereoDispImage(
                left_img_path, right_img_path, disp_img_path)

    def right_path_from_img(self, left_img_path) -> Path:
        return Path(str(left_img_path).replace('left', 'right'))

    @abc.abstractmethod
    def disp_path_from_img(self, path: Path) -> Optional[Path]:
        ...


class SceneFlowCollector(ImageCollector):
    extensions = ['png', 'webp']

    def disp_path_from_img(self, path: Path) -> Optional[Path]:
        match = re.findall(r'.*/(.*?[(?!clean)(?!final)]pass.*?)/', str(path))
        if match:
            disp_path = str(path).replace(match[0], 'disparity')
            disp_path = disp_path.replace(path.suffix, '.pfm')
            return Path(disp_path)
        return None


class MiddleburyCollector(ImageCollector):
    extensions = ['png']

    def _find_images(self) -> Generator[Path, None, None]:
        for ext in self.extensions:
            for img in self.path.rglob(f'im0.{ext}'):
                yield img

    def right_path_from_img(self, left_img_path) -> Path:
        return Path(str(left_img_path).replace('im0', 'im1'))

    def disp_path_from_img(self, path: Path) -> Optional[Path]:
        return path.parent / 'disp0GT.pfm'


if __name__ == '__main__':
    import sys
    images = ImageCollector.load_images(Path(sys.argv[1]))
    images = list(images)
    print(f'{len(images)}')
