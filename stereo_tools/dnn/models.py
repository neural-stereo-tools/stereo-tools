import random
from pathlib import Path

import numpy as np

from ..models import Image, PFMImage, StereoImage


class StereoDispImage(StereoImage):
    disp: PFMImage

    def random_crop(self, crop_height, crop_width):
        height, width, _ = self.left.shape
        crop_x = random.randint(0, width - crop_width)
        crop_y = random.randint(0, height - crop_height)

        left = self.left.data[crop_y:crop_y + crop_height,
                              crop_x:crop_x + crop_width]
        right = self.right.data[crop_y:crop_y + crop_height,
                                crop_x:crop_x + crop_width]
        disp = self.disp.data[crop_y:crop_y + crop_height,
                              crop_x:crop_x + crop_width]

        image_data = np.concatenate([left, right], axis=1)

        pfm = PFMImage.from_image(
            data=disp,
            image=self.disp,
        )
        return StereoDispImage.from_image(
            data=image_data,
            image=self,
            disp=pfm,
        )


class LazyStereoDispImage:
    left_img_path: Path
    right_img_path: Path
    disp_path: Path

    def __init__(self, left, right, disp):
        self.left_img_path = left
        self.right_img_path = right
        self.disp_path = disp

    def load(self) -> StereoDispImage:
        left_img = Image.load(self.left_img_path)
        right_img = Image.load(self.right_img_path)
        disp = PFMImage.load(self.disp_path)

        image_data = np.concatenate([left_img.data, right_img.data], axis=1)
        return StereoDispImage.from_image(
            data=image_data,
            image=left_img,
            disp=disp,
        )
