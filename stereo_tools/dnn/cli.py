from pathlib import Path
from typing import Optional

import click
import scipy.misc

from . import predict as p
from . import train as t
from ..utils import click as types
from ..utils import pfm


@click.command()
@click.option('--show', default=True, type=click.BOOL)
@click.option('--output_file', type=types.PathType(exists=False, writable=True))
@click.argument('network', type=str)
@click.argument('weights_path', type=types.PathType(exists=True, dir_okay=False))
@click.argument('left_img_path', type=types.PathType(exists=True, dir_okay=False))
@click.argument('right_img_path', type=types.PathType(exists=True, dir_okay=False))
def predict(show: bool, network: str, weights_path: Path,
            left_img_path: Path, right_img_path: Path, output_file: Optional[Path]):

    disp = p.predict(
        network, weights_path,
        left_img_path, right_img_path,
    )

    if show:
        scipy.misc.toimage(disp).show()

    if output_file is not None:
        pfm.save_pfm(output_file, disp)


@click.command()
@click.option('--batch_size', type=int)
@click.option('--epochs', type=int)
@click.option('--initial_epoch', type=int)
@click.option('--crop', type=types.TupleIntType(), default=None)
@click.option('--random_size', type=click.BOOL, default=False)
@click.option('--augmentation', type=click.BOOL, default=False)
@click.option('--max_epoch_images', type=int)
@click.option('--weights_path',
              type=types.PathType(exists=True, dir_okay=False, file_okay=True))
@click.argument('network_name', type=str)
@click.argument('output_dir',
                type=types.PathType(exists=True, dir_okay=True, file_okay=False))
@click.argument('datasets', nargs=-1,
                type=types.PathType(exists=True, dir_okay=True, file_okay=False))
def train(*args, **kwargs):
        t.train(*args, **kwargs)
