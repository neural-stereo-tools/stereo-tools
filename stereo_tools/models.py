import json
import logging
from pathlib import Path
from typing import Any, Dict, Optional, Tuple, Type, TypeVar, cast

import cv2
import numpy as np
from pydantic import BaseModel
from scipy.misc import toimage
from skimage.io import imread

from .utils import pfm

logger = logging.getLogger(__name__)
I = TypeVar('I', bound='Image')


class NDArrayType(np.ndarray):
    @classmethod
    def get_validators(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        if not isinstance(value, np.ndarray):
            raise ValueError(f'ndarray expected not {type(value)}.')
        return value


class Image(BaseModel):
    data: NDArrayType
    filename: str
    scope: str = ''

    @property
    def height(self) -> int:
        return self.data.shape[0]

    @property
    def width(self) -> int:
        return self.data.shape[1]

    @property
    def shape(self) -> Tuple[int]:
        return self.data.shape

    @property
    def full_name(self) -> str:
        if self.scope:
            return f'{self.filename} ({self.scope})'
        return self.filename

    def grayscale(self) -> I:
        cls = cast(I, self.__class__)
        return cls.from_image(
            cv2.cvtColor(self.data, cv2.COLOR_BGR2GRAY), self,
            scope='grayscale',
        )

    def show(self) -> None:
        toimage(self.data).show()

    @classmethod
    def load(cls: Type[I], path: Path, **kwargs) -> I:
        data = imread(str(path))
        return cls(data=data, filename=path.name, **kwargs)

    @classmethod
    def from_image(cls: Type[I], data: np.ndarray,
                   image: 'Image' = None, scope: Optional[str]=None,
                   **kwargs) -> I:
        filename = ''
        scope = ''
        if image:
            if scope:
                scopes = [image.scope, scope]
            else:
                scopes = [image.scope]
            scope = ', '.join(scopes)
            filename = image.filename

        return cls(
            data=data,
            filename=filename,
            scope=scope,
            **kwargs
        )


class PFMImage(Image):
    def normalize(self) -> Image:
        # normalize values to be between 0 and 255
        norm_coeff = 255 / self.data.max()
        ndisp = self.data * norm_coeff
        ndisp = ndisp.astype(np.uint8)

        return Image(
            data=ndisp,
            filename=self.filename,
        )

    def show(self):
        self.normalize().show()

    def save(self, path: Path) -> None:
        pfm.save_pfm(path, self.data)

    @classmethod
    def load(cls, path: Path) -> 'PFMImage':
        data, _ = pfm.load_pfm(path)
        return PFMImage(
            data=data,
            filename=path.name,
        )


class StereoImage(Image):
    @property
    def width(self) -> int:
        return int(super().width / 2)

    @property
    def left(self) -> Image:
        data = self.data[0:self.height,
                         0:self.width]
        return Image.from_image(
            data=data,
            image=self,
            scope='left',
        )

    @property
    def right(self) -> Image:
        data = self.data[0:self.height,
                         self.width:2 * self.width]
        return Image.from_image(
            data=data,
            image=self,
            scope='right',
        )


class CameraParameters(BaseModel):
    matrix: NDArrayType
    distortion: NDArrayType

    @property
    def center(self) -> Tuple[int, int]:
        return (self.matrix[0][2],
                self.matrix[1][2])

    @property
    def f(self) -> Tuple[float, float]:
        return (self.matrix[0][0],
                self.matrix[1][1])

    def to_json(self) -> Dict[str, Any]:
        return {
            'matrix': self.matrix.tolist(),
            'distortion': self.distortion.tolist(),
        }

    @staticmethod
    def from_json(json_str) -> 'CameraParameters':
        data = json.loads(json_str)
        return CameraParameters(
            matrix=np.array(data['matrix']),
            distortion=np.array(data['distortion']),
        )


class StereoCamera(BaseModel):
    left: CameraParameters
    right: CameraParameters
    scale: float = 0
    width: int
    height: int

    rotation_matrix: NDArrayType
    translation_matrix: NDArrayType
    essential_matrix: NDArrayType
    fundamental_matrix: NDArrayType

    def get_Q(self, scale=1):
        (_, _, _, _, Q, _, _) = cv2.stereoRectify(
            self.left.matrix, self.left.distortion,
            self.right.matrix, self.right.distortion,
            (self.height, self.width),  # image size
            self.rotation_matrix,
            self.translation_matrix,
            flags=0,
            alpha=0,  # project only valid pixels (after transform)
        )
        Q[3][2] *= 1 / scale
        Q[3][3] *= 1 / scale
        return Q

    def get_focal_length(self):
        Q = self.get_Q()
        return Q[2][3]

    def get_baseline(self, scale=0):
        scale = scale or self.scale

        if scale == 0:
            logger.warning('Scale not specified. The result might not be in meters.')
            return np.fabs(self.translation_matrix).sum()

        Q = self.get_Q()
        return np.fabs(1 / Q[3][2]) * scale

    def to_json(self) -> Dict[str, Any]:
        return {
            'left': self.left.to_json(),
            'right': self.right.to_json(),
            'width': self.width,
            'height': self.height,
            'scale': self.scale,
            'rotation_matrix': self.rotation_matrix.tolist(),
            'translation_matrix': self.translation_matrix.tolist(),
            'essential_matrix': self.essential_matrix.tolist(),
            'fundamental_matrix': self.fundamental_matrix.tolist(),
        }

    @staticmethod
    def from_json(json_str: str) -> 'StereoCamera':
        data = json.loads(json_str)
        return StereoCamera(
            left=CameraParameters.from_json(json.dumps(data['left'])),
            right=CameraParameters.from_json(json.dumps(data['right'])),
            width=data.get('width', 0),
            height=data.get('height', 0),
            scale=data.get('scale', 0),
            rotation_matrix=np.array(data['rotation_matrix']),
            translation_matrix=np.array(data['translation_matrix']),
            essential_matrix=np.array(data['essential_matrix']),
            fundamental_matrix=np.array(data['fundamental_matrix']),
        )

    @staticmethod
    def load(path: Path) -> 'StereoCamera':
        with open(path, mode='rb') as fp:
            return StereoCamera.from_json(fp.read().decode('utf8'))

    def save(self, path: Path):
        with open(path, mode='wb') as fp:
            fp.write(json.dumps(self.to_json()).encode('utf8'))
