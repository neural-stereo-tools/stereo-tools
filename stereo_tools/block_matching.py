import logging

import cv2
import numpy as np

from . import models

logger = logging.getLogger(__name__)


def rectify_image(image: models.StereoImage,
                  camera) -> models.StereoImage:
    """
    Rectifies a image pair using the given camera parameters.
    """
    img_size = (image.width, image.height)
    M1, d1, M2, d2, T, R = camera

    (rotation_matrix_l, rotation_matrix_r,
     projection_matrix_l, projection_matrix_r, _, _, _) = cv2.stereoRectify(
        M1, d1,
        M2, d2,
        img_size,
        R, T,
        flags=0,
        alpha=0,  # project only valid pixels (after transform)
    )

    map_x_left, map_y_left = cv2.initUndistortRectifyMap(
        M1, d1,
        rotation_matrix_l,
        # camera_params.left.new_camera_matrix,  # or use projection matrix
        projection_matrix_l,
        img_size,
        m1type=cv2.CV_32FC1,
    )
    map_x_right, map_y_right = cv2.initUndistortRectifyMap(
        M2, d2,
        rotation_matrix_r,
        projection_matrix_r,
        img_size,
        m1type=cv2.CV_32FC1,
    )

    image_rect_l = cv2.remap(
        image.left.data, map_x_left, map_y_left,
        interpolation=cv2.INTER_CUBIC
    )
    image_rect_r = cv2.remap(
        image.right.data, map_x_right, map_y_right,
        interpolation=cv2.INTER_CUBIC
    )

    return models.StereoImage.from_image(
        np.concatenate((image_rect_l, image_rect_r), axis=1),
        image,
        scope='rectified',
    )


def get_disparity(rect_image: models.StereoImage) -> models.Image:
    """
    Calculates a disparity map.
    """
    # use grayscale image to calucalte disparity map
    rect_image = rect_image.grayscale()

    # compute disparity map
    window_size = 4
    color_channels = 3 if len(rect_image.left.data.shape) == 3 else 1
    min_disp = 0
    num_disp = 192 - min_disp
    stereo = cv2.StereoSGBM_create(
        minDisparity=min_disp,
        numDisparities=num_disp,
        blockSize=window_size,
        P1=8 * color_channels * window_size ** 2,
        P2=32 * color_channels * window_size ** 2,
        disp12MaxDiff=2,
        uniquenessRatio=10,
        speckleWindowSize=0,
        speckleRange=32,
        mode=1,  # use 8 paths
    )

    # calc disparity map
    disp = stereo.compute(
        rect_image.left.data,
        rect_image.right.data,
    ).astype(np.float32) / 16.0

    # all values <0 couldn't be computed, replace them by zero
    disp[disp < 0] = 0

    return models.PFMImage(
        data=disp,
        filename=rect_image.filename,
        scope='disparity',
    )
