from pathlib import Path
from typing import Optional, Tuple

import click
import cv2

from . import block_matching as b
from . import calibrate as c
from . import disparity as d
from . import models
from .utils import click as types
from .utils import pfm, write_ply
from .utils.images import load_images

CHECKERBOARD = (19, 12)


@click.option('--checkerboard', default=str(CHECKERBOARD), type=types.TupleIntType())
@click.option('--extension', default='jpg', type=str)
@click.option('--scale', default=0, type=float)
@click.argument('images_path', type=types.PathType(exists=True, writable=True,
                                                   file_okay=False, dir_okay=True))
@click.argument('output_file', type=types.PathType(exists=False, writable=True))
@click.command()
def calibrate(checkerboard: Tuple[int, int],
              extension: str,
              scale: float,
              images_path: Path,
              output_file: Path) -> None:
    images = list(load_images(images_path, extension=extension))
    parameters = c.calibrate(images, checkerboard)
    parameters.scale = scale
    parameters.save(output_file)
    print('Stored parameters to `{}`'.format(output_file.name))


@click.command()
@click.option('--show', default=True, type=bool)
@click.option('--rectify', default=True, type=bool)
@click.option('--output', type=types.PathType(exists=False, writable=True))
# @click.argument('camera_json', type=types.PathType(exists=True,
#                                                   file_okay=True, dir_okay=False))
@click.argument('left_image', type=types.PathType(exists=True,
                                                  file_okay=True, dir_okay=False))
@click.argument('right_image', type=types.PathType(exists=True,
                                                   file_okay=True, dir_okay=False))
def block_matching(# camera_json: Path,
                   # image_file: Path,
                   left_image: Path,
                   right_image: Path,
                   show: bool = True,
                   rectify: bool = True,
                   output: Optional[Path] = None) -> None:
    import numpy as np
    # camera = models.StereoCamera.load(camera_json)
    limg = models.Image.load(left_image)
    rimg = models.Image.load(right_image)
    image_data = np.concatenate([limg.data, rimg.data], axis=1)
    rect_image = models.StereoImage.from_image(data=image_data, image=limg)
    # if rectify:
    #     rect_image = b.rectify_image(rect_image, camera)
    disp = b.get_disparity(rect_image)

    if show:
        rect_image.show()
        disp.show()

    if output is not None:
        pfm.save_pfm(output, disp.data)


@click.command()
@click.option('--show', default=True, type=bool)
@click.option('--scale', default=0, type=float)
@click.option('--output', type=types.PathType(exists=False, writable=True))
@click.argument('camera_json', type=types.PathType(exists=True, writable=True,
                                                   file_okay=True, dir_okay=False))
@click.argument('disp_file', type=types.PathType(exists=True, writable=True,
                                                 file_okay=True, dir_okay=False))
def depth_map(camera_json: Path,
              disp_file: Path,
              show: bool = True,
              scale: float = 0,
              output: Optional[Path] = None) -> None:
    camera = models.StereoCamera.load(camera_json)
    disp = models.PFMImage.load(disp_file)

    depth = d.create_depth_map(
        disp, camera, scale,
    )

    if show:
        disp.show()
        depth.show()

    if output is not None:
        depth.save(output)


@click.command()
@click.option('--scale', default=0, type=float)
@click.argument('camera_json', type=types.PathType(exists=True, writable=True,
                                                   file_okay=True, dir_okay=False))
@click.argument('disp_file', type=types.PathType(exists=True, writable=True,
                                                 file_okay=True, dir_okay=False))
@click.argument('output', type=types.PathType(exists=False, writable=True))
def pointcloud(camera_json: Path,
               disp_file: Path,
               output: Path,
               scale: float = 0) -> None:
    camera = models.StereoCamera.load(camera_json)
    disp = models.PFMImage.load(disp_file)
    pc = cv2.reprojectImageTo3D(disp.data, camera.get_Q(scale=scale))

    mask = disp.data != 0
    write_ply(output, pc[mask])
