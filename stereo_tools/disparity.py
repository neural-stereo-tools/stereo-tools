import logging

import numpy as np

from . import models

logger = logging.getLogger(__name__)


def depth_map(disp_data, baseline: float, focal_length: float):
    disp_data[disp_data <= 0] = np.inf
    depth_data = focal_length * baseline / disp_data
    depth_data[disp_data == np.inf] = 0

    return depth_data


def create_depth_map(disp: models.PFMImage,
                     camera: models.StereoCamera,
                     scale: float = 0) -> models.Image:
    # baseline and focal_length from rectfied images
    baseline = camera.get_baseline(scale=scale)
    focal_length = camera.get_focal_length()
    disp_data = disp.data.copy()

    depth_data = depth_map(disp_data, baseline, focal_length)
    return models.PFMImage(
        data=depth_data,
        filename=disp.filename,
        scope='depth',
    )
