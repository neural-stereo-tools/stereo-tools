from pathlib import Path
from typing import Generator

from .. import models


def load_images(path: Path,
                extension: str = 'jpg') -> Generator[models.StereoImage, None, None]:
    """
    Loads .jpg files containing SBS images from the given path and
    extracts the images of the left and right camera.

    Returns
        Generator of `StereoImage`
    """
    for img_path in path.glob(f'*.{extension}'):
        yield models.StereoImage.load(img_path)
