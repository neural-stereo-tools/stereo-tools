from pathlib import Path
from typing import Tuple

import click
from click import Path as ClickPathType


class TupleIntType(click.ParamType):
    name = '(int, int)'

    def convert(self, value: str, *args, **kwargs) -> Tuple[int, int]:
        """
        Tries to convert the given value into a tuple of integers
        with length 2.
        """
        try:
            v1, v2 = map(int, value.strip('()').split(','))
        except (IndexError, ValueError):
            self.fail(
                '{} is not a valid tuple of integer with size 2'.format(value))
        return (v1, v2)


class PathType(ClickPathType):
    def convert(self, *args, **kwargs) -> Path:
        path: str = super().convert(*args, **kwargs)
        return Path(path)
