from pathlib import Path

import numpy as np


def get_center(data, width=100):
    center_x = int(data.shape[0] / 2)
    center_y = int(data.shape[1] / 2)
    return data[center_x - width:center_x + width,
                center_y - width:center_y + width]


ply_header = '''ply
format ascii 1.0
element vertex %(vert_num)d
property float x
property float y
property float z
property uchar red
property uchar green
property uchar blue
end_header
'''


def write_ply(output: Path, verts):
    verts = verts.reshape(-1, 3)
    with open(output, 'wb') as f:
        f.write((ply_header % dict(vert_num=len(verts))).encode('utf-8'))
        np.savetxt(f, verts, fmt='%f %f %f 0 0 0')
